use std::error::Error;
use std::fs::File;
use std::io::{BufReader, BufRead, Read};

fn fuel_requirement(mass: i32) -> i32 {
    let req = mass / 3 - 2;
    if req <= 0 {
        0
    } else {
        req + fuel_requirement(req)
    }
}

fn year2019day1() -> Result<i32, Box<dyn Error>>{
    let path = "input/2019/1";
    let handle = File::open(path)?;
    let buf = BufReader::new(handle);
    let mut sum = 0;
    for line in buf.lines() {
        sum += fuel_requirement(line?.parse::<i32>()?);
    }
    Ok(sum)
}

fn grab_val(pos: u32, vec: &mut Vec<u32>) -> u32 {
    vec[vec[pos as usize] as usize]
}

fn intcode_runner(pos: u32, intc: &mut Vec<u32>) -> &mut Vec<u32> {
    if intc[pos as usize] == 99 {
        return intc
    }
    // need to clone the index if we want to modify the vec next
    let index_to_modify = (intc[(pos+3) as usize] as usize).clone();
    intc[index_to_modify] = match intc[pos as usize] {
        1 => grab_val(pos+1, intc) + grab_val(pos+2, intc),
        2 => grab_val(pos+1, intc) * grab_val(pos+2, intc),
        _ => panic!("invalid intcode")
    };
    intcode_runner(pos+4, intc)
}

fn year2019day2input() -> Result<Vec<u32>, Box<dyn Error>> {
    let mut buf = String::new();
    let _ = File::open("input/2019/2")?.read_to_string(&mut buf)?;
    let vals: Result<Vec<_>, _> =
        buf.trim().split(',')
        .map(|val| val.parse::<u32>())
        .collect();
    // this looks a little weird, but it is necessary to avoid a type mismatch
    Ok(vals?)
}

fn year2019day2part1() -> Result<u32, Box<dyn Error>> {
    let mut rvals = year2019day2input()?;
    rvals[1] = 12;
    rvals[2] = 2;
    Ok(intcode_runner(0, &mut rvals)[0])
}

fn year2019day2part2() -> Result<u32, Box<dyn Error>> {
    let vals = year2019day2input()?;
    let mut noun = 0;
    while noun < 100 {
        let mut verb = 0;
        while verb < 100 {
            let mut rvals = vals.clone();
            rvals[1] = noun;
            rvals[2] = verb;
            if intcode_runner(0, &mut rvals)[0] == 19690720 {
                return Ok(100 * noun + verb)
            }
            verb += 1;
        }
        noun += 1;
    }
    Err(From::from("Could not solve".to_string()))
}

fn main() -> Result<(), Box<dyn Error>> {
    let result = year2019day2part2();
    println!("{}", result?);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn twenty_nineteen_day_one() -> Result<(), Box<dyn Error>> {
        assert_eq!(year2019day1()?, 5209504);
        Ok(())
    }
    #[test]
    fn twenty_nineteen_day_two_part_one() -> Result<(), Box<dyn Error>> {
        assert_eq!(year2019day2part1()?, 10566835);
        Ok(())
    }
    #[test]
    fn twenty_nineteen_day_two_part_two() -> Result<(), Box<dyn Error>> {
        assert_eq!(year2019day2part2()?, 2347);
        Ok(())
    }
}
