use std::error::Error;
use anyhow;

#[derive(Debug)]
struct PasswordPolicy {
    min: usize,
    max: usize,
    chr: char,
}

#[derive(Debug)]
struct PasswordDatabaseEntry {
    pol: PasswordPolicy,
    pass: String
}

impl PasswordDatabaseEntry {
    fn is_valid(&self) -> bool {
        let matches = self.pass.matches(self.pol.chr).count();
        self.pol.min <= matches && matches <= self.pol.max
    }

    /* The shopkeeper suddenly realizes that he just accidentally explained the password policy
     * rules from his old job at the sled rental place down the street! The Official Toboggan
     * Corporate Policy actually works a little differently.
     */
    fn is_actually_valid(&self) -> bool {
        let pass_chars: Vec<char> = self.pass.chars().collect();
        let min_matched = match pass_chars.get(self.pol.min - 1) {
            Some(c) => *c == self.pol.chr,
            None => false,
        };
        let max_matched = match pass_chars.get(self.pol.max - 1) {
            Some(c) => *c == self.pol.chr,
            None => false,
        };
        min_matched ^ max_matched
    }
}

fn input_2020_1() -> Result<Vec<u32>, Box<dyn Error>> {
    let vals = advent::input(2020, 1, '\n')?;
    let parsed: Result<Vec<_>, _> = vals
        .iter()
        .map(|val| val.parse::<u32>())
        .collect();
    Ok(parsed?)
}

fn input_2020_2() -> Result<Vec<PasswordDatabaseEntry>, Box<dyn Error>> {
    let list = advent::input(2020, 2, '\n')?;
    let parsed: Result<Vec<PasswordDatabaseEntry>, Box<dyn Error>> = list.iter().map(|item| {
        let pol_pass_vec: Vec<_> = item.split(": ").collect();
        if pol_pass_vec.len() != 2 {
            Err(anyhow::Error::msg("Malformed entry"))?;
        }
        let pol_vec: Vec<_> = pol_pass_vec.get(0)
            .ok_or(anyhow::Error::msg("invalid"))?
            .split(" ").collect();
        let pass = pol_pass_vec.get(1)
            .ok_or(anyhow::Error::msg("invalid"))?;
        if pol_vec.len() != 2 {
            Err(anyhow::Error::msg("Malformed entry"))?;
        }
        let min_max_vec: Vec<_> = pol_vec.get(0)
            .ok_or(anyhow::Error::msg("invalid"))?
            .split("-").collect();
        let chr = pol_vec.get(1)
            .ok_or(anyhow::Error::msg("invalid"))?
            .parse::<char>()?;
        if min_max_vec.len() != 2 {
            Err(anyhow::Error::msg("Malformed entry"))?;
        }
        let min = min_max_vec.get(0)
            .ok_or(anyhow::Error::msg("invalid"))?
            .parse::<usize>()?;
        let max = min_max_vec.get(1)
            .ok_or(anyhow::Error::msg("invalid"))?
            .parse::<usize>()?;
        Ok(PasswordDatabaseEntry{
            pol: PasswordPolicy{min: min, max: max, chr: chr},
            pass: pass.to_string()
        })
    }).collect();
    Ok(parsed?)
}

fn challenge_2020_1_1() -> Result<u32, Box<dyn Error>> {
    let mut nums = input_2020_1()?;
    nums.sort();
    let mut cursor = nums.len() - 1;
    while cursor > 0 {
        let cursor_val = nums.get(cursor).unwrap();
        for num in &nums {
            if num + cursor_val == 2020 {
                return Ok(num * cursor_val)
            }
        }
        cursor -= 1;
    }
    Err(anyhow::Error::msg("No match"))?
}

fn challenge_2020_1_2() -> Result<u32, Box<dyn Error>> {
    let nums = input_2020_1()?;
    for i in &nums {
        for j in &nums {
            for k in &nums {
                if i + j + k == 2020 {
                    return Ok(i * j * k)
                }
            }
        }
    }
    Err(anyhow::Error::msg("No match"))?
}

fn challenge_2020_2_1() -> Result<u32, Box<dyn Error>> {
    let entries = input_2020_2()?;
    let nvalid = entries
        .iter()
        .filter(|e| e.is_valid())
        .count() as u32;
    Ok(nvalid)
}

fn challenge_2020_2_2() -> Result<u32, Box<dyn Error>> {
    let entries = input_2020_2()?;
    let nvalid = entries
        .iter()
        .filter(|e| e.is_actually_valid())
        .count() as u32;
    Ok(nvalid)
}

fn main() -> Result<(), Box<dyn Error>> {
    let o_2020_1_1 = challenge_2020_1_1()?;
    println!("{}", o_2020_1_1);
    let o_2020_1_2 = challenge_2020_1_2()?;
    println!("{}", o_2020_1_2);
    let o_2020_2_1 = challenge_2020_2_1()?;
    println!("{}", o_2020_2_1);
    let o_2020_2_2 = challenge_2020_2_2()?;
    println!("{}", o_2020_2_2);
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_password_validity() -> Result<(), Box<dyn Error>> {
        let entries = vec![
            PasswordDatabaseEntry{
                pol: PasswordPolicy{min: 1, max: 3, chr: 'a'},
                pass: "abcde".to_string(),
            },
            PasswordDatabaseEntry{
                pol: PasswordPolicy{min: 1, max: 3, chr: 'b'},
                pass: "cdefg".to_string(),
            },
            PasswordDatabaseEntry{
                pol: PasswordPolicy{min: 2, max: 9, chr: 'c'},
                pass: "ccccccccc".to_string(),
            },
        ];
        assert_eq!(entries[0].is_valid(), true);
        assert_eq!(entries[1].is_valid(), false);
        assert_eq!(entries[2].is_valid(), true);
        assert_eq!(entries[0].is_actually_valid(), true);
        assert_eq!(entries[1].is_actually_valid(), false);
        assert_eq!(entries[2].is_actually_valid(), false);
        Ok(())
    }
}
