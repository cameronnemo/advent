use std::error::Error;
use std::path::Path;
use std::fs::File;
use std::io::Read;

pub fn input(year: u16, challenge: u8, split: char) -> Result<Vec<String>, Box<dyn Error>> {
    let path = Path::new("input").join(year.to_string()).join(challenge.to_string());
    let mut buf = String::new();
    let _ = File::open(path)?.read_to_string(&mut buf)?;
    let vals = buf
        .trim().split(split)
        .map(|val| val.to_string())
        .collect();
    Ok(vals)
}
